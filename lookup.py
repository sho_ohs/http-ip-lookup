#!/usr/bin/python
import whois11
import re

logfile = "access.log"
with open(logfile) as f:
    logs = f.readlines()

ips = re.findall("(?:[0-9]{1,3}\.){3}[0-9]{1,3}", str(logs))
print(type(ips))
for ip in ips:
    print(ip)
    print(whois11.whois(ip))
